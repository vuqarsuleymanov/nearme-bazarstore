package com.example.bazarstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BazarstoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(BazarstoreApplication.class, args);
    }

}
