package com.example.bazarstore.service;

import com.example.bazarstore.stub.*;
import com.example.bazarstore.xml.XMLHashedParser;
import com.example.bazarstore.xml.XML_Data;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.io.PrintStream;
import java.io.StringReader;
import java.math.BigDecimal;
import java.net.URL;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Aziz Isgandarzada
 */
@Service
@Slf4j
public class ProductService {

    // private final ZETICARET stub;
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    ProductService() throws Exception {
       /* //trustAllCertificates();
        ZETICARET_Service service = new ZETICARET_Service(new URL("http://62.212.236.181:8000/sap/bc/srt/rfc/sap/z_eticaret/110/z_eticaret/z_eticaret_b?wsdl"));
        ZETICARET st = service.getZETICARETB();

        Map<String, Object> context = ((BindingProvider) st).getRequestContext();
        Map<String, List> headers = (Map<String, List>) context.get(MessageContext.HTTP_REQUEST_HEADERS);
        context.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        context.put(BindingProvider.USERNAME_PROPERTY, "WEBSRV_ETC");
        context.put(BindingProvider.PASSWORD_PROPERTY, "Web162636_");
        stub = st;*/
        /*BindingProvider bp = (BindingProvider) st;
       bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "http://62.212.236.181:8000/sap/bc/srt/rfc/sap/z_eticaret/110/z_eticaret/z_eticaret_b");
        bp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "WEBSRV_ETC");
        bp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "Web162636_");*/
    }

    // @PostConstruct
    public void test() throws Exception {
        String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:rfc:functions\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "   <urn:Z_VERI_GONDER/>\n" +
                "   </soapenv:Body>\n" +
                " </soapenv:Envelope>";
        log.info("All Producrs Request Data : " + xml);
        String data = Request.Post("http://62.212.236.181:8000/sap/bc/srt/rfc/sap/z_eticaret/110/z_eticaret/z_eticaret_b")
                .addHeader("Content-Type", "text/xml; charset=utf-8")//
                .addHeader("SOAPAction", "urn:sap-com:document:sap:rfc:functions:Z_ETICARET:Z_VERI_GONDERRequest")//
                .addHeader("Authorization", "Basic V0VCU1JWX0VUQzpXZWIxNjI2MzZf")//
                .bodyString(xml, ContentType.APPLICATION_XML)//
                .execute()//
                .returnContent().asString();
        log.info("All Products Response Data: " + data);
        int begin = data.indexOf("<ET_DATA>");
        int end = data.indexOf("</n0:Z_VERI_GONDERResponse>");
        data = data.substring(begin, end);
        data = XMLHashedParser.parseXmlString(data).getChildByName("ET_DATA").toString();
        XML_Data[] items = XMLHashedParser.parseXmlString(data).getChildByName("ET_DATA").getChildsAsArray("item");
        List<ZTICVERIS> responses = new ArrayList<>();
        for (XML_Data item : items) {
            ZTICVERIS response = new ZTICVERIS();
            response.setBRANDDESCR(item.getChildByName("BRAND_DESCR").getValueUtf8());
            response.setBRANDID(item.getChildByName("BRAND_ID").getValueUtf8());
            response.setEAN11(item.getChildByName("EAN11").getValueUtf8());
            response.setKSCHL(item.getChildByName("KSCHL").getValueUtf8());
            response.setMAKTX(item.getChildByName("MAKTX").getValueUtf8());
            response.setMATNR(item.getChildByName("MATNR").getValueUtf8());
            response.setMEINS(item.getChildByName("MEINS").getValueUtf8());
            response.setZMALGRP(item.getChildByName("ZMALGRP").getValueUtf8());
            response.setZVKP1(new BigDecimal(item.getChildByName("ZVKP1").getValueUtf8()));
            response.setZVKP0(new BigDecimal(item.getChildByName("ZVKP0").getValueUtf8()));
            response.setKSCHL1(item.getChildByName("KSCHL1").getValueUtf8());
            response.setMATKL(item.getChildByName("MATKL").getValueUtf8());
            response.setWAERS(item.getChildByName("WAERS").getValueUtf8());
            response.setZMALGRP1(item.getChildByName("ZMALGRP1").getValueUtf8());
            responses.add(response);
        }
        System.out.println(responses.get(0).getMAKTX());

        ;
        // System.out.println(stub.zVERIGONDER().getItem());
    }

    public List<ZTICVERIS> getProducts() {
        String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:rfc:functions\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "   <urn:Z_VERI_GONDER/>\n" +
                "   </soapenv:Body>\n" +
                " </soapenv:Envelope>";
        try {
            log.info("All Producrs Request Data : " + xml);
            String data = Request.Post("http://62.212.236.181:8000/sap/bc/srt/rfc/sap/z_eticaret/110/z_eticaret/z_eticaret_b")
                    .addHeader("Content-Type", "text/xml; charset=utf-8")//
                    .addHeader("SOAPAction", "urn:sap-com:document:sap:rfc:functions:Z_ETICARET:Z_VERI_GONDERRequest")//
                    .addHeader("Authorization", "Basic V0VCU1JWX0VUQzpXZWIxNjI2MzZf")//
                    .bodyString(xml, ContentType.APPLICATION_XML)//
                    .execute()//
                    .returnContent().asString();
            log.info("All Products Response Data: " + data);
            if (data == null || data.isEmpty()) {
                return null;
            }
            int begin = data.indexOf("<ET_DATA>");
            int end = data.indexOf("</n0:Z_VERI_GONDERResponse>");
            data = data.substring(begin, end);
            XML_Data[] items = XMLHashedParser.parseXmlString(data).getChildByName("ET_DATA").getChildsAsArray("item");
            if (items == null || items.length < 1) {
                return null;
            }
            List<ZTICVERIS> responses = new ArrayList<>();
            for (XML_Data item : items) {
                ZTICVERIS response = new ZTICVERIS();
                response.setBRANDDESCR(item.getChildByName("BRAND_DESCR").getValueUtf8());
                response.setBRANDID(item.getChildByName("BRAND_ID").getValueUtf8());
                response.setEAN11(item.getChildByName("EAN11").getValueUtf8());
                response.setKSCHL(item.getChildByName("KSCHL").getValueUtf8());
                response.setMAKTX(item.getChildByName("MAKTX").getValueUtf8());
                response.setMATNR(item.getChildByName("MATNR").getValueUtf8());
                response.setMEINS(item.getChildByName("MEINS").getValueUtf8());
                response.setZMALGRP(item.getChildByName("ZMALGRP").getValueUtf8());
                response.setZVKP1(new BigDecimal(item.getChildByName("ZVKP1").getValueUtf8()));
                response.setZVKP0(new BigDecimal(item.getChildByName("ZVKP0").getValueUtf8()));
                response.setKSCHL1(item.getChildByName("KSCHL1").getValueUtf8());
                response.setMATKL(item.getChildByName("MATKL").getValueUtf8());
                response.setWAERS(item.getChildByName("WAERS").getValueUtf8());
                response.setZMALGRP1(item.getChildByName("ZMALGRP1").getValueUtf8());
                responses.add(response);
            }
            return responses;
        } catch (Throwable ex) {
            log.info("All Products Error : ", ex);
            return null;
        }
    }


    //@PostConstruct
    public void importSale() {
        String xml = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:tem=\"http://tempuri.org/\">\n" +
                "\t<soap:Header/>\n" +
                "\t<soap:Body>\n" +
                "\t\t<tem:WMob_ImportSale>\n" +
                "\t\t\t<!--Optional:-->\n" +
                "\t\t\t<tem:P_Key>420020103104</tem:P_Key>\n" +
                "\t\t\t<!--Optional:-->\n" +
                "\t\t\t<tem:P_StorageCode>4011</tem:P_StorageCode>\n" +
                "\t\t\t<!--Optional:-->\n" +
                "\t\t\t<tem:P_CashierCode>001</tem:P_CashierCode>\n" +
                "\t\t\t<tem:P_InvoiceID>1</tem:P_InvoiceID>\n" +
                "\t\t\t<tem:P_InvoiceType>1</tem:P_InvoiceType>\n" +
                "\t\t\t<!--Optional:-->\n" +
                "\t\t\t<tem:P_DATETIME>[date]</tem:P_DATETIME>\n" +
                "\t\t\t<!--Optional:-->\n" +
                "\t\t\t<tem:P_Lines>\n" +
                "\t\t\t\t<!--Zero or more repetitions:-->\n" +
                "\t\t\t\t<tem:SaleLines>\n" +
                "\t\t\t\t\t<tem:RecordID>1</tem:RecordID>\n" +
                "\t\t\t\t\t<!--Optional:-->\n" +
                "\t\t\t\t\t<tem:STL_ITEMCODE>?</tem:STL_ITEMCODE>\n" +
                "\t\t\t\t\t<!--Optional:-->\n" +
                "\t\t\t\t\t<tem:STL_ITEMBARCODE>?</tem:STL_ITEMBARCODE>\n" +
                "\t\t\t\t\t<!--Optional:-->\n" +
                "\t\t\t\t\t<tem:STL_UNIT>?</tem:STL_UNIT>\n" +
                "\t\t\t\t\t<tem:STL_QUANTITY>?</tem:STL_QUANTITY>\n" +
                "\t\t\t\t\t<tem:STL_PRICE>?</tem:STL_PRICE>\n" +
                "\t\t\t\t</tem:SaleLines>\n" +
                "\t\t\t</tem:P_Lines>\n" +
                "\t\t</tem:WMob_ImportSale>\n" +
                "\t</soap:Body>\n" +
                "</soap:Envelope>";

        try {
            xml = xml.replace("[date]", sdf.format(new Date()));
            log.info("Import Sale Request Data : " + xml);
            String data = Request.Post("http://almarket.sgofc.com/Srv_AzPos.asmx")
                    .addHeader("Content-Type", "text/xml; charset=utf-8")//
                    .bodyString(xml, ContentType.APPLICATION_XML)//
                    .execute()//
                    .returnContent().asString();
            log.info("Import Sale Response Data: " + data);
        } catch (Throwable ex) {
            log.info("All Products Error : ", ex);
            ex.printStackTrace();
        }

    }


    public static void trustAllCertificates() throws Exception {
        PrintStream ps = System.out;
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
                    throws java.security.cert.CertificateException {
            }

            @Override
            public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
                    throws java.security.cert.CertificateException {
            }

            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
        }};

        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
    }

}
