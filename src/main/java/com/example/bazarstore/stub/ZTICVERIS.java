
package com.example.bazarstore.stub;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ZTIC_VERI_S complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ZTIC_VERI_S">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MATNR" type="{urn:sap-com:document:sap:rfc:functions}char18"/>
 *         &lt;element name="MAKTX" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="ZMALGRP" type="{urn:sap-com:document:sap:rfc:functions}char2"/>
 *         &lt;element name="KSCHL" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="EAN11" type="{urn:sap-com:document:sap:rfc:functions}char18"/>
 *         &lt;element name="ZMALGRP1" type="{urn:sap-com:document:sap:rfc:functions}char4"/>
 *         &lt;element name="KSCHL1" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="MEINS" type="{urn:sap-com:document:sap:rfc:functions}unit3"/>
 *         &lt;element name="BRAND_ID" type="{urn:sap-com:document:sap:rfc:functions}char4"/>
 *         &lt;element name="BRAND_DESCR" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="ZVKP0" type="{urn:sap-com:document:sap:rfc:functions}curr11.2"/>
 *         &lt;element name="WAERS" type="{urn:sap-com:document:sap:rfc:functions}cuky5"/>
 *         &lt;element name="ZVKP1" type="{urn:sap-com:document:sap:rfc:functions}curr11.2"/>
 *         &lt;element name="MATKL" type="{urn:sap-com:document:sap:rfc:functions}char9"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZTIC_VERI_S", propOrder = {
        "matnr",
        "maktx",
        "zmalgrp",
        "kschl",
        "ean11",
        "zmalgrp1",
        "kschl1",
        "meins",
        "brandid",
        "branddescr",
        "zvkp0",
        "waers",
        "zvkp1",
        "matkl"
})
public class ZTICVERIS {

    @XmlElement(name = "MATNR", required = true)
    protected String matnr;
    @XmlElement(name = "MAKTX", required = true)
    protected String maktx;
    @XmlElement(name = "ZMALGRP", required = true)
    protected String zmalgrp;
    @XmlElement(name = "KSCHL", required = true)
    protected String kschl;
    @XmlElement(name = "EAN11", required = true)
    protected String ean11;
    @XmlElement(name = "ZMALGRP1", required = true)
    protected String zmalgrp1;
    @XmlElement(name = "KSCHL1", required = true)
    protected String kschl1;
    @XmlElement(name = "MEINS", required = true)
    protected String meins;
    @XmlElement(name = "BRAND_ID", required = true)
    protected String brandid;
    @XmlElement(name = "BRAND_DESCR", required = true)
    protected String branddescr;
    @XmlElement(name = "ZVKP0", required = true)
    protected BigDecimal zvkp0;
    @XmlElement(name = "WAERS", required = true)
    protected String waers;
    @XmlElement(name = "ZVKP1", required = true)
    protected BigDecimal zvkp1;
    @XmlElement(name = "MATKL", required = true)
    protected String matkl;

    /**
     * Gets the value of the matnr property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMATNR() {
        return matnr;
    }

    /**
     * Sets the value of the matnr property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMATNR(String value) {
        this.matnr = value;
    }

    /**
     * Gets the value of the maktx property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMAKTX() {
        return maktx;
    }

    /**
     * Sets the value of the maktx property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMAKTX(String value) {
        this.maktx = value;
    }

    /**
     * Gets the value of the zmalgrp property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZMALGRP() {
        return zmalgrp;
    }

    /**
     * Sets the value of the zmalgrp property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZMALGRP(String value) {
        this.zmalgrp = value;
    }

    /**
     * Gets the value of the kschl property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getKSCHL() {
        return kschl;
    }

    /**
     * Sets the value of the kschl property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setKSCHL(String value) {
        this.kschl = value;
    }

    /**
     * Gets the value of the ean11 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEAN11() {
        return ean11;
    }

    /**
     * Sets the value of the ean11 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEAN11(String value) {
        this.ean11 = value;
    }

    /**
     * Gets the value of the zmalgrp1 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZMALGRP1() {
        return zmalgrp1;
    }

    /**
     * Sets the value of the zmalgrp1 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZMALGRP1(String value) {
        this.zmalgrp1 = value;
    }

    /**
     * Gets the value of the kschl1 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getKSCHL1() {
        return kschl1;
    }

    /**
     * Sets the value of the kschl1 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setKSCHL1(String value) {
        this.kschl1 = value;
    }

    /**
     * Gets the value of the meins property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMEINS() {
        return meins;
    }

    /**
     * Sets the value of the meins property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMEINS(String value) {
        this.meins = value;
    }

    /**
     * Gets the value of the brandid property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBRANDID() {
        return brandid;
    }

    /**
     * Sets the value of the brandid property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBRANDID(String value) {
        this.brandid = value;
    }

    /**
     * Gets the value of the branddescr property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBRANDDESCR() {
        return branddescr;
    }

    /**
     * Sets the value of the branddescr property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBRANDDESCR(String value) {
        this.branddescr = value;
    }

    /**
     * Gets the value of the zvkp0 property.
     *
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *
     */
    public BigDecimal getZVKP0() {
        return zvkp0;
    }

    /**
     * Sets the value of the zvkp0 property.
     *
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *
     */
    public void setZVKP0(BigDecimal value) {
        this.zvkp0 = value;
    }

    /**
     * Gets the value of the waers property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getWAERS() {
        return waers;
    }

    /**
     * Sets the value of the waers property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setWAERS(String value) {
        this.waers = value;
    }

    /**
     * Gets the value of the zvkp1 property.
     *
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *
     */
    public BigDecimal getZVKP1() {
        return zvkp1;
    }

    /**
     * Sets the value of the zvkp1 property.
     *
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *
     */
    public void setZVKP1(BigDecimal value) {
        this.zvkp1 = value;
    }

    /**
     * Gets the value of the matkl property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMATKL() {
        return matkl;
    }

    /**
     * Sets the value of the matkl property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMATKL(String value) {
        this.matkl = value;
    }

}
