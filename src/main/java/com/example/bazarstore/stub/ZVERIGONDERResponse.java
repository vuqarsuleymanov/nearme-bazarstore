
package com.example.bazarstore.stub;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ET_DATA" type="{urn:sap-com:document:sap:rfc:functions}ZTIC_VERI_T"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "etdata"
})
@XmlRootElement(name = "Z_VERI_GONDERResponse")
public class ZVERIGONDERResponse {

    @XmlElement(name = "ET_DATA", required = true)
    protected ZTICVERIT etdata;

    /**
     * Gets the value of the etdata property.
     *
     * @return
     *     possible object is
     *     {@link ZTICVERIT }
     *
     */
    public ZTICVERIT getETDATA() {
        return etdata;
    }

    /**
     * Sets the value of the etdata property.
     *
     * @param value
     *     allowed object is
     *     {@link ZTICVERIT }
     *
     */
    public void setETDATA(ZTICVERIT value) {
        this.etdata = value;
    }

}
