
package com.example.bazarstore.stub;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.example.bazarstore.stub package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.example.bazarstore.stub
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ZVERIGONDERResponse }
     *
     */
    public ZVERIGONDERResponse createZVERIGONDERResponse() {
        return new ZVERIGONDERResponse();
    }

    /**
     * Create an instance of {@link ZTICVERIT }
     *
     */
    public ZTICVERIT createZTICVERIT() {
        return new ZTICVERIT();
    }

    /**
     * Create an instance of {@link ZVERIGONDER }
     *
     */
    public ZVERIGONDER createZVERIGONDER() {
        return new ZVERIGONDER();
    }

    /**
     * Create an instance of {@link ZTICVERIS }
     *
     */
    public ZTICVERIS createZTICVERIS() {
        return new ZTICVERIS();
    }

}
