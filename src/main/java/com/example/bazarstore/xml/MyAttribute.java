package com.example.bazarstore.xml;

import java.io.Serializable;

public class MyAttribute implements Serializable {
    private static final long serialVersionUID = 2544815931384206980L;
    private String name;
    private String value;

    public static MyAttribute createAttribute(String name, String value) {
        MyAttribute a = new MyAttribute();
        a.name = name;
        a.value = value;
        return a;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}

