package com.example.bazarstore.xml;

import org.xml.sax.Attributes;

import java.io.Serializable;

/**
 * Class for storing parsed xml document. This class contain one taq(node) of
 * xml document and it additional datas.
 * <p/>
 * <p/>
 * example:
 * {@code
 * XML_Data data = XMLHashedParser.parse(XML_File).getChildByName("service").getChildByName("database");
 * StringBuffer ConnString = new StringBuffer();
 * ConnString.append(data.getChildByName("host").getValue()).append("?user=").append(data.getChildByName("user").getValue()).append("&password=").append(data.getChildByName("password").getValue());
 * return ConnString.toString();
 * }
 * <p/>
 * <p/>
 * <p/>
 * User: elmar Date: May 11, 2008
 */
public class XML_Data implements Serializable {
    private static final long serialVersionUID = -6692966615379722919L;
    private String name = "";
    private String value = "";
    private XML_Data[] childs;
    private XML_Data parent;
    private int childcount;
    private MyAttribute[] attributes = new MyAttribute[0];

    private boolean isEmpty = true;

    private static XML_Data emptyInstanse = new XML_Data(0);

    /**
     * consructor
     */
    public XML_Data() {
        this(16);
    }

    public XML_Data(int i) {
        childs = new XML_Data[i];
        childcount = 0;
    }

    /**
     * method to get current nodes name
     *
     * @return current nodes name
     */
    public String getName() {
        return name;
    }

    /**
     * methot to set current nodes name
     *
     * @param name
     */
    public void setName(String name) {
        isEmpty = false;
        this.name = name;
    }

    /**
     * methot to get current nodes text value(content)
     *
     * @return current nodes text value(content)
     */
    public String getValue() {
        return value;
    }

    public String getValueUtf8() {
        try
        {
            if(value == null) return null;
            return new String(value.getBytes(), "UTF-8");
        }
        catch(Exception ex)
        {
            return value;
        }
    }

    /**
     * methot to set current nodes text value(content)
     */
    public void setValue(String value) {
        isEmpty = false;
        this.value = value;
    }

    /**
     * add new child node to current node
     *
     * @param child
     *            node
     */
    public void AddChilds(XML_Data child) {
        isEmpty = false;
        child.setParent(this);
        if (this.childs.length <= childcount)
            extendArray(childcount * 2);

        this.childs[childcount++] = child;
        optimize();
    }

    /**
     * method to get child nodes of current node
     *
     * @return array of nodes
     */
    public XML_Data[] getChilds() {
        return childs;
    }

    /**
     * method to get array of childs only one type.
     *
     * @param name
     *            - name of searchin child nodes
     * @return array of child nodes by name
     */
    public XML_Data[] getChildsAsArray(String name) {
        XML_Data[] res = new XML_Data[getChildCount(name)];
        int id = 0;
        for (int i = 0; i < childcount; i++)
            if (childs[i].name.equalsIgnoreCase(name))
                res[id++] = childs[i];
        return res;
    }

    /**
     * method to get count of childs only one type.
     *
     * @param name
     *            - name of searchin child nodes
     * @return count of childs only one type.
     */
    public int getChildCount(String name) {
        int count = 0;
        for (int i = 0; i < childcount; i++)
            if (childs[i].name.equalsIgnoreCase(name))
                count++;
        return count;
    }

    /**
     * method to get first child by name
     *
     * @param name
     *            - child name
     * @return child node
     */
    public XML_Data getChildByName(String name) {
        return getChildByName(name, 1);
    }

    /**
     * @param name
     *
     */
    public XML_Data getChildNodeSequense(String name) {
        XML_Data res = this;
        String[] names = name.split("\\.");
        for (int i = 0; i < names.length; i++)
            res = res.getChildByName(names[i]);

        return res;
    }

    /**
     * method for get id's node by name
     *
     * @param name
     *            child name
     * @param id
     *            start from 0
     * @return child node
     */
    public XML_Data getChildByNameFromZero(String name, int id) {
        return getChildByName(name, id + 1);
    }

    /**
     * @deprecated use getChildByNameFromZero id start from 1
     */
    @Deprecated
    public XML_Data getChildByName(String name, int id) {
        int findid = id;
        for (int i = 0; i < childcount; i++)
            if (childs[i].name.equalsIgnoreCase(name) && findid-- == 1)
                return childs[i];
        return XML_Data.emptyInstanse;
    }

    /**
     * checks is child by name exist
     *
     * @param name
     * @param id
     * @return true id child exist. else false
     */
    public boolean hasChildByName(String name, int id) {
        int findid = id;
        for (int i = 0; i < childcount; i++)
            if (childs[i].name.equalsIgnoreCase(name) && findid-- == 1)
                return true;
        return false;
    }

    private void extendArray(int i) {
        XML_Data[] tmp = new XML_Data[i];
        System.arraycopy(childs, 0, tmp, 0, childcount);
        childs = tmp;
    }

    /**
     * optimize structure
     */
    public void optimize() {
        XML_Data[] tmp = new XML_Data[childcount];
        System.arraycopy(childs, 0, tmp, 0, childcount);
        childs = tmp;
    }

    /**
     * method to get parent node of this node.
     *
     * @return parent node or null if this id root node.
     */
    public XML_Data getParent() {
        return parent;
    }

    /**
     * sets the parent node
     *
     * @param parent
     *            node
     */
    public void setParent(XML_Data parent) {
        isEmpty = false;
        this.parent = parent;
    }

    /**
     * @return child count of this node.
     */
    public int getChildCount() {
        return childcount;
    }

    /**
     * create xml from this model. not tested.
     *
     * @return xml string
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        toString(sb);
        return sb.toString();
    }

    private void toString(StringBuilder sb) {
        sb.append("<" + name);

        for (int i = 0; i < attributes.length; i++)
            sb.append(" ").append(attributes[i].getName()).append("=\"")
                    .append(attributes[i].getValue()).append("\"");
        if (value.equals("") && childs.length == 0) {
            sb.append(" />");
            return;
        } else
            sb.append(">");

        if (childcount != 0)
            for (int i = 0; i < childcount; i++)
                childs[i].toString(sb);
        else
            sb.append(value);
        sb.append("</").append(name).append(">");
    }

    /**
     * @return array of attribules of this node
     */
    public MyAttribute[] getAttributes() {
        return attributes;
    }

    /**
     * get attribute value by name
     *
     * @param name
     *            atribute value
     * @return atribute value string
     */
    public String getAttributeByName(String name) {
        for (int i = 0; i < attributes.length; i++)
            if (attributes[i].getName().equals(name))
                return attributes[i].getValue();
        return null;
    }

    /**
     * set attribute value
     *
     * @param a
     *            - attribute
     */
    public void setAttributes(Attributes a) {
        this.attributes = new MyAttribute[a.getLength()];
        for (int i = 0; i < attributes.length; i++)
            attributes[i] = MyAttribute.createAttribute(a.getQName(i),
                    a.getValue(a.getQName(i)));
    }

    public void setAttributes(MyAttribute[] attributes) {
        this.attributes = attributes;
    }

    /**
     * if node is not found(for example child node) metod returns empty node. It
     * prvent null pointer exceptions
     *
     * @return true if this node is empty node.
     */
    public boolean isEmpty() {
        return isEmpty;
    }
}

