package com.example.bazarstore.xml;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;


/**
 * Class for parsing xml documents.
 * <p/>
 * {@code
 * example:
 * XML_Data data = XMLHashedParser.parse(XML_File).getChildByName("service").getChildByName("database");
 * StringBuffer ConnString = new StringBuffer();
 * ConnString.append(data.getChildByName("host").getValue()).append("?user=").append(data.getChildByName("user").getValue()).append("&password=").append(data.getChildByName("password").getValue());
 * return ConnString.toString();
 * }
 * <p/>
 * <p>
 * <p>
 * User: elmar Date: May 11, 2008
 */
public class XMLHashedParser extends DefaultHandler implements Serializable {
    private static final long serialVersionUID = -3693049324094339659L;
    public XML_Data data;
    private XML_Data realdata;
    StringBuffer CurrData;

    @Override
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {
        CurrData = new StringBuffer();

        XML_Data tmp = new XML_Data();
        tmp.setName(qName);
        tmp.setAttributes(attributes);
        realdata.AddChilds(tmp);
        realdata = tmp;

    }

    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        realdata.setValue(CurrData.toString().trim());
        CurrData = new StringBuffer();
        realdata.optimize();
        realdata = realdata.getParent();

    }

    @Override
    public void characters(char ch[], int start, int length)
            throws SAXException {
        CurrData.append(ch, start, length);
    }

    private XMLHashedParser() {
        data = new XML_Data();
        data.setName("root");
        data.setValue("");
        realdata = data;
    }

    /**
     * parsing xml String
     *
     * @param XmlString
     * @return parsed xml data
     * @throws ParserConfigurationException
     * @throws SAXException                 happen if xml cannot be parsed
     * @throws IOException
     */
    public static XML_Data parseXmlString(String XmlString)
            throws ParserConfigurationException, SAXException, IOException {
        InputSource is = new InputSource(new StringReader(XmlString));
        is.setEncoding("UTF-8");
        return parse(is);
    }

    /**
     * parsing xml file/
     *
     * @param XMLfile address
     * @return parsed xml data
     * @throws ParserConfigurationException
     * @throws SAXException                 happen if xml cannot be parsed
     * @throws IOException                  happen if there any problem with address of file
     */
    public static XML_Data parse(String XMLfile)
            throws ParserConfigurationException, SAXException, IOException {
        return parse(new InputSource(new FileInputStream(XMLfile)));
    }

    /**
     * parsing xml from InputSource
     *
     * @param XMLFileSource input sourse
     * @return parsed xml data
     * @throws ParserConfigurationException
     * @throws SAXException                 happen if xml cannot be parsed
     * @throws IOException                  happen if there any problem with InputSource
     */
    public static XML_Data parse(InputSource XMLFileSource)
            throws ParserConfigurationException, SAXException, IOException {
        XMLHashedParser parser = new XMLHashedParser();
        XMLReader myparser = SAXParserFactory.newInstance().newSAXParser()
                .getXMLReader();
        myparser.setContentHandler(parser);
        myparser.parse(XMLFileSource);
        parser.data.optimize();
        return parser.data;
    }

    /**
     * parse mysql connection xml file
     *
     * @param XML_File connection file address
     * @return connection string
     * @throws IOException                  happen if there any problem with address of file
     * @throws ParserConfigurationException
     * @throws SAXException                 happen if xml cannot be parsed
     */
    public static String CreateConnection(String XML_File) throws IOException,
            ParserConfigurationException, SAXException {
        XML_Data data = XMLHashedParser.parse(XML_File)
                .getChildByName("service").getChildByName("database");

        StringBuffer ConnString = new StringBuffer();
        ConnString.append(data.getChildByName("host").getValue())
                .append("?user=")
                .append(data.getChildByName("user").getValue())
                .append("&password=")
                .append(data.getChildByName("password").getValue());
        return ConnString.toString();
    }

}

