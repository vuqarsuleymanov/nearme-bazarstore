package com.example.bazarstore.controller;

import com.example.bazarstore.service.ProductService;
import com.example.bazarstore.stub.ZTICVERIS;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("product")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("all")
    public List<ZTICVERIS> getProducts() {
        return productService.getProducts();
    }

}
